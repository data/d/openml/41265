# OpenML dataset: Titanic

https://www.openml.org/d/41265

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The goal is to predict the Fare. 
 Variable description:
                                          pclass: A proxy for socio-economic status (SES)
                                          1st = Upper
                                          2nd = Middle
                                          3rd = Lower

                                          age: Age is fractional if less than 1. If the age is estimated, is it in the form of xx.5

                                          sibsp: The dataset defines family relations in this way...
                                          Sibling = brother, sister, stepbrother, stepsister
                                          Spouse = husband, wife (mistresses and fiances were ignored)
                                          
                                          parch: The dataset defines family relations in this way...
                                          Parent = mother, father
                                          Child = daughter, son, stepdaughter, stepson
                                          Some children travelled only with a nanny, therefore parch=0 for them

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41265) of an [OpenML dataset](https://www.openml.org/d/41265). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41265/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41265/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41265/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

